#include <thrust/device_vector.h>
#include <thrust/for_each.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/host_vector.h>
#include <thrust/generate.h>
#include <thrust/complex.h>
#include <thrust/gather.h>
#include <thrust/scatter.h>
#include <cufft.h>

#include <stdio.h>
#include <stdlib.h>
//#include <math.h>

#include "globalCpp.h"


struct Timer_cuda
{
  cudaEvent_t estart, estop;
  Timer_cuda()
  {
    cudaEventCreate(&estart);
    cudaEventCreate(&estop);
  }
  ~Timer_cuda()
  {
    cudaEventDestroy(estart);
    cudaEventDestroy(estop);
  }
  void start()
  {
     cudaEventRecord(estart, 0);
  }
  float stop()
  {//time in seconds
    cudaEventRecord(estop, 0);
    cudaEventSynchronize(estop);
    float time;
    cudaEventElapsedTime(&time, estart, estop);
    return time/1000;
  }
};

typedef Timer_cuda tc;
enum {trans_t, fft_x_t, fft_y_t, fft_z_t, fft_xyz_t, checksum_t, verify_t,
    ic_t, evolve_t, coeffs_t, constr_fft_t, ic_copy_t, shuffle_t};
const int timers_num = 20;
float timers[timers_num];
typedef thrust::complex<double> complex;
typedef unsigned int uint;

struct DivideFunctor
{
    uint N;
    DivideFunctor(uint NN)
    : N(NN)
    {}
    __device__ __host__ void operator()(complex & v)
    {
        v /= N;
    }
};

void evolve_cuda(thrust::device_vector<complex> & v, thrust::device_vector<complex> & w, thrust::device_vector<complex> & coeffs)
{
  thrust::transform(v.begin(), v.end(), coeffs.begin(), v.begin(), thrust::multiplies<complex>());
  w = v;
}


struct CoeffFunctor
{
    const double ap = -4.0 * ALPHA * (PI * PI);
    CoeffFunctor()
    {}
    __device__ __host__ double operator()(const uint & id)
    {
        const uint n12 = NX / 2, n22 = NY / 2, n32 = NZ / 2;
        const uint i = id % NX, j = ((id - i) / NX) % NY, k = (((id - i) / NX) - j) / NY;
      	const int ii = i - (i / n12) * NX;
      	const int jj = j - (j / n22) * NY;
      	const int kk = k - (k / n32) * NZ;
        return exp(ap * (ii*ii + jj*jj + kk*kk));
    }
};


void calculate_coeffs_cuda(thrust::device_vector<complex> & coeffs)
{
   CoeffFunctor cf;
   thrust::counting_iterator<uint> first(0);
   thrust::counting_iterator<uint> last = first + NX*NY*NZ;
   thrust::transform(first, last, coeffs.begin(), cf);
}

struct CalculateChecksum_cuda
{

  thrust::device_vector<uint> map;
  uint den;
  CalculateChecksum_cuda(uint d1, uint d2, uint d3)
  : map(1024), den(d1*d2*d3)
  {
    thrust::host_vector<uint> map_host(1024);
    for (uint i = 0; i < 1024; i++) {
      const uint i1 = i+1;
      const uint ii = i1 % d1;
      const uint ji = 3 * i1 % d2;
      const uint ki = 5 * i1 % d3;
      map_host[i] = ii + (ji + ki * d2) * d1;
    }
    map = map_host;
  }

  void calculateChecksum(dcomplex *csum, uint iterN, thrust::device_vector<complex> & w)
  {
    complex csum_temp = thrust::reduce(thrust::make_permutation_iterator(w.begin(), map.begin()),
                                       thrust::make_permutation_iterator(w.begin(), map.end()));
    //csum_temp /= den;
    printf(" T =%5d     Checksum =%22.12E%22.12E\n", 
        iterN, csum_temp.real(), csum_temp.imag());

    *csum = dcmplx(csum_temp.real(), csum_temp.imag());
  }
};

void compute_initial_conditions_cuda(uint d1, uint d2, uint d3, 
                                     thrust::device_vector<complex> & V)
{
  dcomplex tmp[MAXDIM];
  double x0, start, an;//dummy;
  double RanStarts[MAXDIM];

  uint i, j, k;
  const double seed = 314159265.0;
  const double a = 1220703125.0;

  start = seed;
  //---------------------------------------------------------------------
  // Jump to the starting element for our first plane.
  //---------------------------------------------------------------------
  an = ipow46(a, 0);
  randlc(&start, an);
  an = ipow46(a, 2*d1*d2);
  //---------------------------------------------------------------------
  // Go through by z planes filling in one square at a time.
  //---------------------------------------------------------------------
  RanStarts[0] = start;
  for (k = 1; k < d3; k++) {
    randlc(&start, an);
    RanStarts[k] = start;
  }

  thrust::host_vector<complex> v_host(V.size());

  for (k = 0; k < d3; k++) {
    x0 = RanStarts[k];
    for (j = 0; j < d2; j++) {
      vranlc(2*d1, &x0, a, (double *)tmp);
      for (i = 0; i < d1; i++) {
        //u0[k][j][i] = tmp[i];
        v_host[i + (j + k*d2)*d1] = complex(tmp[i].real, tmp[i].imag);
      }
    }
  }
  tc copy_ti;
  copy_ti.start();
  V = v_host;
  timers[ic_copy_t] += copy_ti.stop();
}

enum fft_type {FORWARD_FFT, INVERSE_FFT};

struct FFTXYZ_cuFFT
{
  uint nx, ny, nz;
  cufftHandle plan;
  FFTXYZ_cuFFT(uint nx, uint ny, uint nz)
  : nx(nx), ny(ny), nz(nz)
  {
    cufftPlan3d(&plan, nz, ny, nx, CUFFT_Z2Z);
  }
  ~FFTXYZ_cuFFT()
  {
    cufftDestroy(plan);
  }
  void compute(fft_type type, thrust::device_vector<complex> & data,
                   thrust::device_vector<complex> & output)
  {
    if (type == FORWARD_FFT) {
      cufftExecZ2Z(plan, (cuDoubleComplex*)thrust::raw_pointer_cast(data.data()),
            (cuDoubleComplex*)thrust::raw_pointer_cast(output.data()), CUFFT_FORWARD);

      DivideFunctor divideFunctor(nx*ny*nz);
      thrust::for_each(output.begin(), output.end(), divideFunctor);
    } else {
      cufftExecZ2Z(plan, (cuDoubleComplex*)thrust::raw_pointer_cast(data.data()),
            (cuDoubleComplex*)thrust::raw_pointer_cast(output.data()), CUFFT_INVERSE);
    }
  }
};

//---------------------- CUSTOM FFT ----------------------
//__constant__ cuDoubleComplex fwd_glob[1024], inv_glob[1024];



// convert a linear index to a linear index in the transpose 
struct transpose_index : public thrust::unary_function<size_t,size_t>
{
    const size_t m, n, l;

    __host__ __device__
    transpose_index(size_t _m, size_t _n, size_t _l) : m(_m), n(_n), l(_l) {}

    __device__
    size_t operator()(const size_t & linear_index)
    {
        const size_t i = linear_index & (m-1);
        const size_t j_p_kn = linear_index / m;

        return j_p_kn + i * n * l;
    }
};


struct transpose_index_gather_v : public thrust::unary_function<size_t,size_t>
{
    const size_t m, n, l;

    uint shift;
    __host__ __device__
    transpose_index_gather_v(size_t _m, size_t _n, size_t _l)
    : m(_m), n(_n), l(_l)
    {
        uint nn = n;
        shift = 0;
        while (nn) {
            shift++;
            nn = nn << 1;
        }
    }

    __device__
    size_t operator()(size_t linear_index)
    {
        const size_t j = linear_index & (n-1);
        linear_index = linear_index - j + ( __brev(j) >> shift );

        const size_t i = linear_index / (n*l);
        const size_t j_p_kn = linear_index & (n*l - 1);

        return i + j_p_kn * m;
    }
};

struct TransposeIJK2JKI
{
    const complex * from;
    complex * to;
    TransposeIJK2JKI(const complex * a, complex *b)
    : from(a), to(b)
    {}
    __device__  void operator()(const uint & id)
    {
        //const uint i = id % NX, j = ((id - i) / NX) % NY, k = (((id - i) / NX) - j) / NY;
        //to[j + (k + i * NZ) * NY] = from[id];
        const uint i = id % NX, j_p_kNY = id/NX;
        to[j_p_kNY + i * NZ*NY] = from[id];
    }
};

struct TransposeJKI2KIJ
{
    const complex * from;
    complex * to;
    TransposeJKI2KIJ(const complex * a, complex * b)
    : from(a), to(b)
    {}
    __device__ void operator()(const uint & id)
    {
        //const uint j = id % NY, k = ((id - j) / NY) % NZ, i = (((id - j) / NY) - k) / NZ;
        //to[k + (i + j * NX) * NZ] = from[id];
        const uint  j = id % NY, k_p_iNZ = id/NY;
        to[k_p_iNZ + j * NX*NZ] = from[id];
    }
};

struct TransposeKIJ2IJK
{
    const complex * from;
    complex * to;
    TransposeKIJ2IJK(const complex * a, complex * b)
    : from(a), to(b)
    {}
    __device__ void operator()(const uint & id)
    {
        //const uint k = id % NZ, i = ((id - k) / NZ) % NX, j = (((id - k) / NZ) - i) / NX;
        //to[i + (j + k * NY) * NX] = from[id];
        const uint k = id % NZ, i_p_jNX = id/NZ;
        to[i_p_jNX + k * NX*NY] = from[id];
    }
};


/*
struct InitBitsPerm
{
    uint N;
    uint log_N;
    InitBitsPerm(uint NN)
    : N(NN), log_N(round(log2(NN)))
    {}
    template <typename Tuple>
    __device__ __host__ void operator()(Tuple & t)
    {
        const uint i = thrust::get<1>(t);
        uint v = 0;
        for (uint j = 0; j < log_N; j++)
            if (i & (1 << j))
                v |= 1 << (log_N - 1 - j);
        thrust::get<0>(t) = v;
    }
    template <typename Tuple>
    __device__ void operator()(Tuple & t)
    {
        const uint i = thrust::get<1>(t);
        thrust::get<0>(t) = (__brev(i) >> (32 - log_N) );
    }
};

struct PerformPerm
{
    complex *dev_data;
    __host__ __device__
    PerformPerm(complex *dev_data)
    : dev_data(dev_data)
    {}
    template <typename Tuple>
    __device__ __host__ void operator()(const Tuple & t)
    {
        uint perm_i, i;
        thrust::tie(perm_i, i) = t;
        if (i < perm_i)
            thrust::swap(dev_data[i], dev_data[perm_i]);
    }
};
*/
struct NewPerformPerm
{
    complex *dev_data;
    const uint n;
    uint shift;
    __host__ __device__
    NewPerformPerm(complex *dev_data, int N)
    : dev_data(dev_data), n(N)
    {
        uint nn = n;
        shift = 0;
        while (nn) {
            shift++;
            nn = nn << 1;
        }
    }
    __device__ void operator()(const uint & ii)
    {
        const uint i = ii & (n-1);
        const uint offset = ii - i;
        const uint perm_i =  (__brev(i) >> shift );
        if (i < perm_i) {
          thrust::swap(dev_data[ii], dev_data[offset + perm_i]);
        }
    }
};

struct EasyNewPerformPerm: public thrust::unary_function<size_t,size_t>
{
    const uint n;
    uint shift;
    __host__ __device__
    EasyNewPerformPerm(uint N)
    : n(N)
    {
        uint nn = n;
        shift = 0;
        while (nn) {
            shift++;
            nn = nn << 1;
        }
    }
    __device__ size_t operator()(size_t ii)
    {
        const uint i = ii & (n - 1);
        const uint perm_i =  (__brev(i) >> shift );
        return ii - i + perm_i;
    }
};


struct transpose_n_shuffle : public thrust::unary_function<size_t,size_t>
{ // [m, n, l] -> [n, l, m] then shuffle
    //really slow
    uint m, n, l;
    uint shift;
    __host__ __device__
    transpose_n_shuffle(uint _m, uint _n, uint _l)
    : m(_m), n(_n), l(_l)
    {
        uint nn = n;
        shift = 0;
        while (nn) {
            shift++;
            nn = nn << 1;
        }
    }

    __device__
    size_t operator()(uint linear_index)
    {
        const uint i = linear_index & (m - 1);
        const uint j = (linear_index / m) & (n - 1);
        const uint k = linear_index / (m*n);
        const uint perm_i =  ( __brev(j) >> shift );
        return n * (k + i*l) + perm_i;
    }
};


struct BF
{
    complex * const a;
    const complex * const ww;
    const uint len2, n2;

    __host__ __device__
    BF(complex * a, const complex * ww, const uint & len, const uint & n)
    : a(a), ww(ww), len2(len/2), n2(n/2)
    {}
    __device__ void operator()(const uint & ijk)
    {
        //const uint j = (ijk % n2) % len2;
        const uint j = ijk & (n2-1) & (len2-1);
        const uint offset = 2 * ijk - j;
        complex * const ax = a + offset;
        complex * const ay = ax + len2;

        const complex t = ww[j] * *ay;
        *ay = *ax - t;
        *ax += t;
    }
};

struct FindTwiddles
{
    const complex wlen;
    FindTwiddles(const complex & wlen)
    : wlen(wlen)
    {}
    template <typename Tuple>
    __device__ __host__ void operator()(Tuple & t)
    {
        thrust::get<0>(t) = thrust::pow(wlen, thrust::get<1>(t));
    }
};


void bfs(uint n, uint vecnum, const complex * factors, complex * a)
{
    const uint par2 = vecnum * n / 2;
    thrust::counting_iterator<uint> first(0);
    for (uint len = 2; len <= n; len *= 2) {
        thrust::for_each_n(first, par2, BF(a, factors, len, n));
        factors += (len/2);
    }
}

void shuffle(uint n, uint vecnum, complex * a)
{
    thrust::counting_iterator<uint> first(0);
    thrust::for_each_n(first, n * vecnum, NewPerformPerm(a, n));
}

void shuffle_easy(uint n, const thrust::device_vector<complex> & from,
        thrust::device_vector<complex> & to)
{//looks like a bit slower than in-place
    thrust::counting_iterator<size_t> first(0);
    thrust::gather
    (thrust::make_transform_iterator(first, EasyNewPerformPerm(n)),
    thrust::make_transform_iterator(first, EasyNewPerformPerm(n)) +
    from.size(),
    from.begin(), to.begin());
}

struct FFTXYZ_CUSTOM
{
  //performs 3d FFT transform

  uint nx, ny, nz, max_d;
 // thrust::device_vector<uint> rev_x, rev_y, rev_z;
  thrust::device_vector<complex> factors_fwd, factors_inv;
//  uint * rev_x_p, * rev_y_p, * rev_z_p;
  complex * factors_fwd_p, * factors_inv_p;
public:
  FFTXYZ_CUSTOM(uint nx, uint ny, uint nz)
  : nx(nx), ny(ny), nz(nz),// rev_x(nx), rev_y(ny), rev_z(nz),
    max_d(max(nx, max(ny, nz))),
    factors_fwd(max_d + 32), factors_inv(max_d + 32),
   // rev_x_p(thrust::raw_pointer_cast(rev_x.data())),
  //  rev_y_p(thrust::raw_pointer_cast(rev_y.data())),
  //  rev_z_p(thrust::raw_pointer_cast(rev_z.data())),
    factors_fwd_p(thrust::raw_pointer_cast(factors_fwd.data())),
    factors_inv_p(thrust::raw_pointer_cast(factors_inv.data()))
  {
      /*
      thrust::counting_iterator<uint> first(0);
      {
          InitBitsPerm initBP(nx);
          thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(rev_x.begin(), first)),
                           thrust::make_zip_iterator(thrust::make_tuple(rev_x.end(), first + nx)),
                           initBP);
      }
      {
          InitBitsPerm initBP(ny);
          thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(rev_y.begin(), first)),
                           thrust::make_zip_iterator(thrust::make_tuple(rev_y.end(), first + ny)),
                           initBP);
      }
      {
          InitBitsPerm initBP(nz);
          thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(rev_z.begin(), first)),
                           thrust::make_zip_iterator(thrust::make_tuple(rev_z.end(), first + nz)),
                           initBP);
      }
     */
        const int padding = 1;
      for (uint len = 2, pos = padding; len <= max_d; len *= 2) {
          const double ang_fwd = - 2 * M_PI / len;
          const double ang_inv = - ang_fwd;
          uint len2 = len / 2;

          FindTwiddles findTwiddles_fwd(complex(cos(ang_fwd), sin(ang_fwd))),
                       findTwiddles_inv(complex(cos(ang_inv), sin(ang_inv)));

          thrust::counting_iterator<uint> first(0);
          thrust::counting_iterator<uint> last = first + len2;
          thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(factors_fwd.begin() + pos, first)),
                           thrust::make_zip_iterator(thrust::make_tuple(factors_fwd.begin() + pos + len2, last)),
                           findTwiddles_fwd);
          thrust::for_each(thrust::make_zip_iterator(thrust::make_tuple(factors_inv.begin() + pos, first)),
                           thrust::make_zip_iterator(thrust::make_tuple(factors_inv.begin() + pos + len2, last)),
                           findTwiddles_inv);
          pos += len2;
      }
    /*
      cudaMemcpyToSymbol(fwd_glob, factors_fwd_p,
                max_d*sizeof(cuDoubleComplex));
        cudaMemcpyToSymbol(inv_glob, factors_inv_p,
                max_d*sizeof(cuDoubleComplex));
        void *devicePtr = NULL;
        cudaGetSymbolAddress(&devicePtr, fwd_glob);
        factors_fwd_p = (complex*) devicePtr;
        cudaGetSymbolAddress(&devicePtr, inv_glob);
        factors_inv_p = (complex*) devicePtr;
	*/////!!!!!!!!!!!!!!!!!!!!!!!!!

    factors_fwd_p += padding;
	factors_inv_p += padding;

  }
  ~FFTXYZ_CUSTOM()
  {}
  void my_checksum(thrust::device_vector<complex> &in)
  {
    complex csum_temp = thrust::reduce(in.begin(), in.end());
    printf("CHECK: %lf, %lf\n", csum_temp.real(), csum_temp.imag());

  }
  void compute(fft_type type, thrust::device_vector<complex> & input,
                   thrust::device_vector<complex> & output)
  {
    //timers
    tc fft_x_ti, fft_y_ti, fft_z_ti, transpose_ti;// shuffle_ti;
    complex * input_raw = thrust::raw_pointer_cast(input.data());
    complex * output_raw = thrust::raw_pointer_cast(output.data());
    thrust::counting_iterator<uint> first(0);

    thrust::counting_iterator<size_t> indices(0);
    const uint total_n = nx*ny*nz;
    //same steps for forward and inverse

    // 1. transpose i,j,k <-> j,k,i and shuffle in output
    
    transpose_ti.start();
   /* 
    thrust::scatter
    (input.begin(), input.end(),
    thrust::make_transform_iterator(indices, transpose_index(nx, ny, nz)),
    output.begin());
    */
    thrust::gather
    (thrust::make_transform_iterator(indices, transpose_index_gather_v(nx, ny, nz)),
    thrust::make_transform_iterator(indices, transpose_index_gather_v(nx, ny, nz)) +
    total_n,
    input.begin(),output.begin());

    timers[trans_t] += transpose_ti.stop();

    //shuffle_ti.start();
    
    //shuffle(ny, nx*nz, output_raw);
    
    //timers[shuffle_t] += shuffle_ti.stop();
    

    // 2. do fft over j in output
    
    fft_y_ti.start();
    
    bfs(ny, nx*nz, (type == FORWARD_FFT ? factors_fwd_p : factors_inv_p),
            output_raw);
    
    timers[fft_y_t] += fft_y_ti.stop();
    
my_checksum(output);

    // 3. transpose j,k,i <-> k,i,j and shuffle in input
     
    transpose_ti.start();
/*
    thrust::scatter
    (output.begin(), output.end(),
    thrust::make_transform_iterator(indices, transpose_index(ny, nz, nx)),
    input.begin());
*/
    thrust::gather
    (thrust::make_transform_iterator(indices, transpose_index_gather_v(ny, nz, nx)),
    thrust::make_transform_iterator(indices, transpose_index_gather_v(ny, nz, nx)) +
    total_n,
    output.begin(),input.begin());

    timers[trans_t] += transpose_ti.stop();


    //shuffle_ti.start();
    
    //shuffle(nz, nx*ny, input_raw);
    
    //timers[shuffle_t] += shuffle_ti.stop();
    
    // 4. do fft over k in input
    
    fft_z_ti.start();

    bfs(nz, nx*ny, (type == FORWARD_FFT ? factors_fwd_p : factors_inv_p),
                     input_raw);

    timers[fft_z_t] += fft_z_ti.stop();
    
    // 5. transpose k,i,j <-> i,j,k and shuffle in output
    
    transpose_ti.start();
/*
    thrust::scatter
    (input.begin(), input.end(),
    thrust::make_transform_iterator(indices, transpose_index(nz, nx, ny)),
    output.begin());
  */
    thrust::gather
    (thrust::make_transform_iterator(indices, transpose_index_gather_v(nz, nx,
                                                                       ny)),
    thrust::make_transform_iterator(indices, transpose_index_gather_v(nz, nx,
            ny)) +
    total_n,
    input.begin(),output.begin());

    timers[trans_t] += transpose_ti.stop();
   

    //shuffle_ti.start();
    
    //shuffle(nx, ny*nz, output_raw); 

    //timers[shuffle_t] += shuffle_ti.stop();

    // 3. do fft over i in output

    fft_x_ti.start();

    bfs(nx, ny*nz, (type == FORWARD_FFT ? factors_fwd_p : factors_inv_p),
            output_raw);
    
    timers[fft_x_t] += fft_x_ti.stop();
    
    //done

    if (type == FORWARD_FFT) {
      DivideFunctor divideFunctor(nx*ny*nz);
      thrust::for_each(output.begin(), output.end(), divideFunctor);
    }
  }



};

//---------------------- END CUSTOM FFT ----------------------

#define CUSTOM_FFT_
void appft_cuda(int niter, double *total_time, logical *verified)
{
  int nDevices;

  cudaGetDeviceCount(&nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("alignment: %d\n", prop.textureAlignment);
    printf("Device Number: %d\n", i);
    printf("  Device name: %s\n", prop.name);
    printf("  Memory Clock Rate (KHz): %d\n",
           prop.memoryClockRate);
    printf("  Memory Bus Width (bits): %d\n",
           prop.memoryBusWidth);
    printf("  Peak Memory Bandwidth (GB/s): %f\n\n",
           2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6);
  }

  cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);
  tc total_ti;
  total_ti.start();

  tc evolve_ti, fft_xyz_ti, checksum_ti, coeffs_ti, ic_ti;
  tc constr_fft;
  for (int i = 0; i < timers_num; i++) {
    timers[i] = 0;
  }

  // for checksum data
  dcomplex sums[NITER_DEFAULT+1];
  thrust::device_vector<complex> V(NX*NY*NZ), W(NX*NY*NZ), W2(NX*NY*NZ), coeffs(NX*NY*NZ);

  constr_fft.start();
#ifdef CUSTOM_FFT_
  FFTXYZ_CUSTOM fft_xyz(NX, NY, NZ);
  printf("************* CUSTOM_FFT *************\n");
#else
  printf("**************** cuFFT ****************\n");
  FFTXYZ_cuFFT fft_xyz(NX, NY, NZ);
#endif
  timers[constr_fft_t] += constr_fft.stop();
  CalculateChecksum_cuda calcChecksum_cuda(NX, NY, NZ);

  

  if (timers_enabled) coeffs_ti.start();
//coeffs
  calculate_coeffs_cuda(coeffs);
  if (timers_enabled) timers[coeffs_t] += coeffs_ti.stop();

  if (timers_enabled) ic_ti.start();
  compute_initial_conditions_cuda(NX, NY, NZ, W);
  if (timers_enabled) timers[ic_t] += ic_ti.stop();
printf("Checksum of initial conditions\n");
calcChecksum_cuda.calculateChecksum(&sums[0], 0, W);
  if (timers_enabled) fft_xyz_ti.start();
  fft_xyz.compute(FORWARD_FFT, W, V);
  if (timers_enabled) timers[fft_xyz_t] += fft_xyz_ti.stop();
printf("Checksum of the forward fft\n");
calcChecksum_cuda.calculateChecksum(&sums[0], 0, V);
  for (int kt = 1; kt <= niter; kt++) {
    
    if (timers_enabled) evolve_ti.start();
    evolve_cuda(V, W, coeffs);
    if (timers_enabled) timers[evolve_t] += evolve_ti.stop();
    
    if (timers_enabled) fft_xyz_ti.start();
    fft_xyz.compute(INVERSE_FFT, W, W2);
    if (timers_enabled) timers[fft_xyz_t] += fft_xyz_ti.stop();

    if (timers_enabled) checksum_ti.start();
    calcChecksum_cuda.calculateChecksum(&sums[kt], kt, W2);
    if (timers_enabled) timers[checksum_t] += checksum_ti.stop();
  }

  // Verification test.
  tc ver;
  if (timers_enabled) ver.start();
  verify(NX, NY, NZ, niter, sums, verified);
  if (timers_enabled) timers[verify_t] += ver.stop();

  printf("cudaGetErrorString: %s\n", cudaGetErrorString(cudaGetLastError()));
  if (!timers_enabled) return;
  printf(" FT subroutine timers \n");
  printf(" %26s =%9.4f\n", "Benchmark time            ", total_ti.stop());
  printf(" %26s =%9.4f\n", "fftXYZ                    ", timers[fft_xyz_t]);
  printf(" %26s =%9.4f\n", "  Transpose&shuffle       ", timers[trans_t]);
 // printf(" %26s =%9.4f\n", "  Shuffle                 ", timers[shuffle_t]);
  printf(" %26s =%9.4f\n", "  Total 1d fft            ", timers[fft_x_t] +
          timers[fft_y_t] + timers[fft_z_t]);
  printf(" %26s =%9.4f\n", "    X time                ", timers[fft_x_t]);
  printf(" %26s =%9.4f\n", "    Y time                ", timers[fft_y_t]);
  printf(" %26s =%9.4f\n", "    Z time                ", timers[fft_z_t]);
  printf(" %26s =%9.4f\n", "CalculateChecksum         ", timers[checksum_t]);
  printf(" %26s =%9.4f\n", "coefficients              ", timers[coeffs_t]);
  printf(" %26s =%9.4f\n", "evolve                    ", timers[evolve_t]);
  printf(" %26s =%9.4f\n", "compute_initial_cond      ", timers[ic_t]);
  printf(" %26s =%9.4f\n", "  copying init cond only  ", timers[ic_copy_t]);
  printf(" %26s =%9.4f\n", "FFT constructor           ", timers[constr_fft_t]);
  printf(" %26s =%9.4f\n", "verify                    ", timers[verify_t]);
}

