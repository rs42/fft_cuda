#include "npbparams.h"
#include "type.h"

#define SEED                  314159265.0
#define A                     1220703125.0
#define PI                    3.141592653589793238
#define ALPHA                 1.0e-6

/* common /timerscomm/ */
extern logical timers_enabled;


#define dcmplx(r,i)       (dcomplex){r, i}
#define dcmplx_add(a,b)   (dcomplex){(a).real+(b).real, (a).imag+(b).imag}
#define dcmplx_sub(a,b)   (dcomplex){(a).real-(b).real, (a).imag-(b).imag}
#define dcmplx_mul(a,b)   (dcomplex){((a).real*(b).real)-((a).imag*(b).imag),\
                                     ((a).real*(b).imag)+((a).imag*(b).real)}
#define dcmplx_mul2(a,b)  (dcomplex){(a).real*(b), (a).imag*(b)}

#define dcmplx_div2(a,b)  (dcomplex){(a).real/(b), (a).imag/(b)}
#define dcmplx_abs(x)     sqrt(((x).real*(x).real) + ((x).imag*(x).imag))

#define dconjg(x)         (dcomplex){(x).real, -1.0*(x).imag}

extern "C" double ipow46(double a, int exponent);
extern "C" void appft_cuda(int niter, double *total_time, logical *verified);
extern "C" void verify(int n1, int n2, int n3, int nt, dcomplex cksum[],
            logical *verified);
extern "C" double randlc( double *x, double a );
extern "C" void vranlc( int n, double *x, double a, double y[] );

extern "C" void timer_clear( int n );
extern "C" void timer_start( int n );
extern "C" void timer_stop( int n );
extern "C" double timer_read( int n );
